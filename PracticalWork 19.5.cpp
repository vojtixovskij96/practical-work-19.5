﻿
#include <iostream>
using namespace std;
class Animal
{
public:
    virtual void Voice() 
    {
        std::cout << "1 ";
    }
};
class Dog : public Animal
{
    void Voice()  override
    {
        cout << "Woof!" << '\n';
    }
};
class Cat : public Animal
{
     void Voice()override
    {
        cout << "Meow" << '\n';
    }
};
class Duck :public Animal
{
    void Voice()  override
    {
        cout << "Crack" << '\n';
    }
};
int main()
{
    Animal* animals[3];
    animals[0] = new Dog;
    animals[1] = new Cat;
    animals[2] = new Duck;

    for (Animal*a:animals)
    
        a->Voice();
        
    
   
}



